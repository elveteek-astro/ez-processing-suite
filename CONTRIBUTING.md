# Contributing to the EZ Processing Suite

Thank you for considering contributing to the EZ Processing Suite! Your contributions are valuable and help improve the suite for the benefit of the PixInsight astrophotography community.

Before you get started, please take a moment to read and understand the following guidelines.

## How to Contribute

1. Fork this repository to your GitHub account.
2. Create a branch for your contribution. Choose a descriptive name for your branch.
3. Make your changes or additions to the codebase, documentation, or other project assets.
4. Test your changes thoroughly to ensure they work as expected.

## Commit Guidelines

- Write clear, descriptive commit messages that explain the purpose and scope of your changes.
- Use [conventional commit messages](https://www.conventionalcommits.org/) when applicable, especially for version bumping and release notes generation.
- Reference relevant issues or pull requests in your commit messages using keywords like "Fixes," "Closes," or "Resolves."

## Submitting Changes

1. Push your changes to your forked repository.
2. Create a merge request (MR) in this repository's GitLab. Please include the following details in your MR:
   - A clear title and description of your changes.
   - Any relevant information about your changes.
   - Reference any related issues or pull requests.
3. Be prepared to respond to feedback and make necessary adjustments to your contribution.

## Code Style Guidelines

- Follow the existing code style and formatting conventions in the project.
- If you're adding new code, ensure it adheres to the established coding standards.
- Maintain a consistent coding style throughout your contribution.

## Testing

- Ensure that your code changes do not introduce regressions.
- Write tests for new code or changes to existing functionality where applicable.
- Make sure all existing tests continue to pass.

## Documentation

- If you make changes that affect the project's documentation, please update the documentation accordingly.
- Keep documentation clear, concise, and up to date.

## Licensing

- Ensure that your contributions comply with the licenses of the project and any third-party dependencies.
- If you're introducing new dependencies, be aware of their licenses and compatibility with the project's license.

## Code of Conduct

By participating in this project, you agree to abide by the [Code of Conduct](CODE_OF_CONDUCT.md). Please report any unacceptable behavior to [project maintainers](#contact).

## Contact

If you have questions, suggestions, or need assistance, please feel free to reach out to the project maintainers through the GitLab issues section of this repository.

Thank you for contributing to the EZ Processing Suite! Your contributions make a difference.
