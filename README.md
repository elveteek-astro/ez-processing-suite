# EZ Processing Suite Repository

Welcome to the EZ Processing Suite repository! This repository is dedicated to the EZ Processing Suite, originally written by S. Dimant. Elveteek Std is now providing a PixInsight update repository for this suite.

## Overview

The EZ Processing Suite is a powerful set of tools and scripts designed to enhance your astrophotography processing workflow within PixInsight. With the help of this repository, you can access updates and improvements to the suite, making it even more efficient and user-friendly.

## Getting Started

To get started with the EZ Processing Suite, you can visit the official update repository provided by Elveteek Sàrl:

**Update Repository URL:** [https://elveteek.ch/pixinsight-updates/ez-processing-suite/](https://elveteek.ch/pixinsight-updates/ez-processing-suite/)

**Installation documentation:** [https://elveteek.ch/pixinsight/ez-processing-suite/](https://elveteek.ch/pixinsight/ez-processing-suite/)

If you still have the previous version installed you need to remove it manually.
To do so, delete the previous version files in the Pixinsight installation directory and restart PixInsight:
```
<PixInsight dir>\src\scripts\EZ_Common.js
<PixInsight dir>\src\scripts\EZ_Decon.js
<PixInsight dir>\src\scripts\EZ_HDR.js
<PixInsight dir>\src\scripts\EZ_LiveStack.js
<PixInsight dir>\src\scripts\EZ_SoftStretch.js
<PixInsight dir>\src\scripts\EZ_StarReduction.js
<PixInsight dir>\src\scripts\RistaMask_UI.js
```

## Current Release

The update repository currently provides a release for PixInsight version 1.8.9-1. Please ensure that you have the appropriate version of PixInsight installed to make the most of the EZ Processing Suite's features.

## Contributions

Contributions to the EZ Processing Suite repository are very welcome! Whether you have bug fixes, feature enhancements, or new scripts to contribute, your efforts will help improve the suite and benefit the PixInsight astrophotography community.

To contribute, please follow these steps:

1. Fork this repository.
2. Create a branch for your contribution.
3. Make your changes or additions.
4. Test your changes to ensure they work as expected.
5. Commit your changes with clear and descriptive commit messages.
6. Push your changes to your forked repository.
7. Create a merge request to merge your changes into the main repository.

Please review our [Contribution Guidelines](CONTRIBUTING.md) for more details on the contribution process.

## License

The EZ Processing Suite is distributed under the [MIT License](LICENSE). You can find the full text of the license in the [LICENSE](LICENSE) file.

## Contact

If you have any questions, suggestions, or need assistance, please feel free to contact the maintainers or contributors through the GitLab issues section of this repository.

Thank you for using the EZ Processing Suite, and we look forward to your contributions!
