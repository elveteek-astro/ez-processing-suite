# Code of Conduct

## Introduction

The EZ Processing Suite project is committed to creating a welcoming and inclusive community for all contributors, regardless of their background or identity. We expect all participants, including but not limited to contributors, maintainers, and users, to adhere to this Code of Conduct to help ensure a positive and respectful environment.

## Our Pledge

In the interest of fostering an open and welcoming community, we pledge to:

- Be respectful and considerate of diverse opinions, experiences, and backgrounds.
- Be inclusive and welcoming to all, regardless of race, ethnicity, nationality, religion, gender, gender identity, sexual orientation, disability, age, or any other characteristic.
- Show empathy towards others and assume good intentions.

## Unacceptable Behavior

The following behaviors are considered unacceptable within our community:

- Harassment, discrimination, or intimidation in any form.
- Offensive or derogatory comments, slurs, or jokes.
- Personal attacks, trolling, or unwarranted hostility.
- Violating someone's privacy, including sharing personal information without consent.
- Deliberately excluding or making others feel unwelcome.

## Reporting and Enforcement

If you experience or witness behavior that violates this Code of Conduct, we encourage you to report it promptly to the project maintainers. You can contact us through the GitLab issues section of this repository or via email at [contact@example.com].

All reports will be kept confidential, and your identity will be protected to the extent permitted by law.

The project maintainers are committed to addressing and resolving reported incidents promptly and fairly. We may take appropriate actions, which could include warnings, temporary or permanent bans, or other measures necessary to maintain a welcoming and respectful environment.

## Attribution

This Code of Conduct is adapted from the [Contributor Covenant](https://www.contributor-covenant.org/version/2/1/code_of_conduct.html), version 2.1.

## Thank You

Thank you for contributing to and participating in the EZ Processing Suite community. Your adherence to this Code of Conduct helps ensure a positive and inclusive environment for everyone.

Please note that by participating in this project, you are expected to follow this Code of Conduct in all project-related spaces, including but not limited to GitLab repositories, discussion forums, and communication channels.
